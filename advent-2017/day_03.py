class Node:
    def __init__(self, n, t=None, r=None, b=None, l=None):
        self.num = n
        self.top = t
        self.right = r
        self.bottom = b
        self.left = l

    def __str__(self):
        return '%s' % self.n

#  y
# x0
# 
#  y
# x0 1
#  1 2

class Graph:

    def __init__(self, rows, columns):
        self.cell_size = max([len(str(rows)), len(str(columns))])
        # self.graph = [range(columns) for row in range(rows)]
        self.graph = []
        for row in range(rows):
            c_row = []
            for cell in range(columns):
                c_row.append('X')
            self.graph.append(c_row)

        print(self)

    def __str__(self):
        rows = []
        for row in self.graph:
            rows.append(' '.join([str(cell).center(self.cell_size) for cell in row]) + '\n')
        return ''.join(rows)

    def get(x,y):
        return self.graph[x][y]

directions = ['right', 'up', 'left', 'down']
def next_direction(direction, directions):
    if direction + 1 >= len(directions):
        return directions[(direction + 1) - len(directions)]
    return directions[direction+1]




direction = 0

def build_spiral(upper_bound):
    """
    17  16  15  14  13
    18   5   4   3  12
    19   6   1   2  11
    20   7   8   9  10
    21  22  23---> upper_bound

    1

    1 2

      3
    1 2

    4 3
    1 2

    5 4 3
      1 2

    5 4 3
    6 1 2

    4 5 3
    6 1 2
    7

    4 5 3
    6 1 2
    7 8

    4 5 3
    6 1 2
    7 8 9

    go right until end of graph
    go up    until end of graph
    go left  until end of graph
    go down  until end of graph
    go right until end of graph

    right, up, left, down
    """
    c_x = 0
    c_y = 0
    g = Graph(1,1)
    for i in range(1, upper_bound):
        num = i+1
        if directions[direction] == 'right':
            g.graph[c_x][c_y+1] = num

build_spiral(5)
